import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Carousel from '../components/Carousel'


export default function Home() {
	return(
		<Fragment>
		  <Banner/>
		  <Carousel/>
		  <Highlights/>
		</Fragment>
		)
}