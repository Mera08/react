import Container from 'react-bootstrap/Container';
import CreateProduct from './CreateProduct';
import DashboardHeader from '../components/DashboardHeader' 
import Dashboard from './Dashboard'


export default function Admin() {


	return(
		<Container>
		  <DashboardHeader/>
		  <Dashboard/>
		</Container>
		)
}