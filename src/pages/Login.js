import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false)
	const {user, setUser} = useContext(UserContext);

	useEffect(()=>{
		if (email !== "" && password !==""){
				setIsActive(true);
		}
		else{
			setIsActive(false)
		}
	}, [email,password])

	function loginUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_URI}/users/login`, { // fetch a request to the corresponding api
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email, // :email is the state hooks
				password: password
			})
		}).then(response => response.json())
		.then(data =>{

			console.log(data)

			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed :(",
					icon: "error",
					text: "Check your login details and try again."
				})
				setPassword('');
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}})
			.then(response=> response.json())
			.then(data =>{
				console.log(data);
				setUser({id: data._id, isAdmin: data.isAdmin});
			})
		}
	}

	return(

		(user.id !== null) 
		?	(user.isAdmin === true)
			?	<Navigate to="/admin" />
			:
			<Navigate to="/" />
		:
		
		
		<div>
		<div class="d-flex flex-md-row flex-column justify-content-md-around mt-2 pt-5 pb-3">
		<h1>Login</h1>
		</div>
		<div class="d-flex flex-md-row flex-column justify-content-md-around pb-3">
		<h4 className='text-secondary'>Don't have an account yet? <a href="/register" className='text-decoration-none text-dark'><em> Create account</em></a></h4>
		</div>
			<Row>
				<Col className=" col-md-4 col-8 offset-md-4 offset-2 p-3">
				
					<Form onSubmit={loginUser} className=" p-3 text-white">
					  
					  <Form.Group className="mb-3" controlId="email">
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email" 
					    	value = {email}
					    	onChange  = {event => setEmail(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="password">
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange  = {event => setPassword(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Button 
					  	  className="text-white align-items-center w-100"
						  variant="dark" 
						  type="submit"
						  id="submitBtn"
						  disabled={!isActive}
						  >Login
					  </Button>
					</Form>
				
				</Col>
			</Row>
		</div>	
		
		)
}